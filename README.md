# 9Digital coding challenge

A few things to note when reviewing this challenge.

First of all, thanks for taking the time out of your day to review this. 😁

1. I spent around 3 hours implementing this, this includes about 30 minutes of pre-planning and writing out TODO's
2. I have used the Dependency Inversion principle throughout this small service as I believe it's highly scalable as it enforces good testability and once understood, enables developers to move quite fast.
   1. To do this, I used Inversify, which is a dependency injection framework, and makes binding and injecting quite simple.
3. I would like to clarify that initially, I couldn't decide how overboard to take this test, but the requirements document at http://codingchallenge.nine.com.au/ stated this should be production ready, so I put the extra effort it in making sure it followed good principles and was scaffolded for the future.
4. I have tried to make very few assumptions relating to how the data being given is formed.
   1. This is why you will find in the root endpoint there are quite a few checks. The document did not specify if fields like `Payload` were always going to exist, so I made the assumption they _might_ not, and added in some defensive measures.
5. I have used TypeScript throughout this as in my career I have found it be absolutely amazing, to say the least! It lets you move fast and feel comfortable doing so.
6. I have deployed this to a server hosted on an EC2 instance on AWS.
   1. If you're interested in how my setup works (self plug), check out the article I wrote at https://www.koryporter.com/2019/07/21/using-aws-docker-nginx-and-subdomain-routing-to-host-all-of-your-node-apps-for-free.

## Pre Requisites

- Node 12

## Setup

`yarn install`

`touch .env`

Set the PORT variable if you want to have it run on anything other than 3000.

## TODO

- [x] get folder structure sorted
  - src
    - services
      - ShowService.ts
      - JSONService.ts
    - endpoints
      - index.ts
    - config
      - inversify.ts
    - helpers
      - ...
- [x] get dependency injection set up (inversify)
- [x] set up API using invserify-express-utils (makes dependency injection and express a breeze!)
- [-] get scripts set up
  - [x] build
  - [x] run
  - [] test
- [x] define type structure for incoming data (json2ts is awesome for this)
- [x] define type structure for outgoing expectant data (image, slug, title, etc)
- [x] define interface for JSONService
  - public methods
    - validateJSONString(jsonString: string): boolean;
      - unit test this
      - it should THROW the syntax error if unparseable
      - validate json, simplest way is to try catch JSON.parse
- [x] build out tests for JSONService based on the interface (TDD)
- [x] build out JSONService
- [x] define interface for ShowService
  - private methods
    - checkShowHasDrm(show: IShow): boolean;
      - unit test this
    - checkShowHasEpisodes(show: IShow, minEpisodesRequired?: number): boolean;
      - unit test this
  - public methods
    - filterShows(shows: IShow[]): IDrmEnabledShows[];
      - unit test this
- [x] build out tests for ShowService based on the interface (TDD)
- [x] build out ShowService
- [x] build out the simple endpoint

  - flow

    - validate body object (as the tech challenge says they will POST the data)
    - if invalid, return a 400 with the following

      ```js
      {
        "error": "Could not decode request: JSON parsing failed"
      }
      ```

    - otherwise continue on and call the ShowService.filterShows(show) method
    - return the filtered shows.
