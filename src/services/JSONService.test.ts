import "reflect-metadata";

import test from "ava";
import { JSONService } from "./JSONService";

test("validateJSONString throws if invalid stringified JSON is passed", t => {
  t.plan(2);

  const s = new JSONService();

  t.throws(() =>
    s.validateJSONString(
      "testing 123, this is bad json { hello: 'foo bar baz'}"
    )
  );

  t.throws(() =>
    s.validateJSONString("{ test: true, invalid: 'yes', cats: error }")
  );
});

test("validateJSONString throws if stringified numbers are passed", t => {
  t.plan(3);

  const s = new JSONService();

  t.throws(() => s.validateJSONString("1234"));

  // scientific numbers
  t.throws(() => s.validateJSONString("1111e1"));

  // floating point
  t.throws(() => s.validateJSONString("1.2222222333"));
});

test("validateJSONString returns parsed JSON if the input JSON string is valid", t => {
  t.plan(2);

  const s = new JSONService();

  const validJSONInputOne = {
    foo: "bar",
    baz: "barry",
    plants: ["monstera", "ivy", "string of pearls"]
  };

  const validJSONInputTwo = {
    foo: 5,
    baz: {},
    empty: [],
    invalid: null,
    plants: [
      {
        name: "monstera",
        sunLightRequirement: 8
      },
      {
        name: "rubber tree",
        sunLightRequirement: 3
      }
    ]
  };

  const resOne = s.validateJSONString(JSON.stringify(validJSONInputOne));
  const resTwo = s.validateJSONString(JSON.stringify(validJSONInputTwo));

  t.deepEqual(resOne, validJSONInputOne);
  t.deepEqual(resTwo, validJSONInputTwo);
});
