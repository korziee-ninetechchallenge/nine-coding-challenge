import { injectable } from "inversify";
import { IShow } from "../types/IShow";
import { IFilteredShow } from "../types/IFilteredShow";

export interface IShowService {
  /**
   * Filters down a list of show's based on their DRM setting
   * having at least one episode
   */
  filterShows(shows: IShow[]): IFilteredShow[] | null;
}

@injectable()
export class ShowService implements IShowService {
  constructor() {}

  /**
   * Helper methods for ShowService.
   *
   * Even though these are "public" in the world of TypeScript, they are not defined on the interface,
   * therefore any other "services" that utilise this service, will only depend on the methods/properties
   * defined on the interface, as is a common practice when following dependency inversion.
   *
   * @see https://en.wikipedia.org/wiki/Dependency_inversion_principle for more information
   */

  public static checkShowHasDrm(show: IShow): boolean {
    return show && typeof show.drm === "boolean" && show.drm === true;
  }

  public static checkShowHasEpisodes(
    show: IShow,
    minEpisodes: number
  ): boolean {
    return (
      show &&
      typeof show.episodeCount === "number" &&
      show.episodeCount >= minEpisodes
    );
  }

  public static formatShow(show: IShow): IFilteredShow {
    return {
      title: show.title,
      slug: show.slug,
      image: show.image.showImage
    };
  }

  /**
   * Publicly available and interface provided methods.
   */

  public filterShows(shows: IShow[]): IFilteredShow[] | null {
    if (!shows || !Array.isArray(shows)) {
      return null;
    }

    return shows
      .filter(
        s =>
          ShowService.checkShowHasDrm(s) &&
          // should have atleast one episode
          ShowService.checkShowHasEpisodes(s, 1)
      )
      .map(ShowService.formatShow);
  }
}
