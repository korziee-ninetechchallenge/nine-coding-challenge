import { injectable } from "inversify";

export interface IJSONService {
  /**
   * Validates and returns a parsed object.
   *
   * @throws `SyntaxError` or `Error` if there is invalid stringified JSON provided
   */
  validateJSONString(jsonString: string): any;
}

@injectable()
export class JSONService implements IJSONService {
  constructor() {}

  public validateJSONString(jsonString: string): any {
    try {
      const parsedJSON = JSON.parse(jsonString);

      /**
       * Validate that the parsed json is indeed object-like object/array/etc
       * We do this because `JSON.parse("1234")` will respond with the number primitive with a value of 1234
       */
      if (typeof parsedJSON !== "object") {
        throw new SyntaxError();
      }

      // no validation errors occured, return the parsed JSON.
      return parsedJSON;
    } catch (e) {
      throw e;
    }
  }
}
