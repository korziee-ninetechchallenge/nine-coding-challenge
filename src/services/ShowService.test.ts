import "reflect-metadata";

import test from "ava";
import { ShowService } from "./ShowService";
import { IShow } from "../types/IShow";

const getDefaultShow = (): IShow => ({
  title: "A show about, testing -- it usually takes longer!",
  slug: "show-about-testing",
  tvChannel: "9WEST"
});

test("checkShowHasDrm does not error if nothing is passed in", t => {
  t.falsy(ShowService.checkShowHasDrm(null));
  // @ts-ignore
  t.falsy(ShowService.checkShowHasDrm({}));
  // @ts-ignore
  t.falsy(ShowService.checkShowHasDrm(10));
});

test("checkShowHasDrm only returns shows that have DRM enabled", t => {
  const shows: IShow[] = [
    {
      ...getDefaultShow(),
      drm: true
    },
    {
      ...getDefaultShow(),
      drm: false
    },
    {
      ...getDefaultShow(),
      drm: null
    },
    {
      ...getDefaultShow()
    },
    {
      ...getDefaultShow(),
      drm: undefined
    },
    {
      ...getDefaultShow(),
      // @ts-ignore
      drm: 1
    }
  ];
  t.true(ShowService.checkShowHasDrm(shows[0]));
  t.false(ShowService.checkShowHasDrm(shows[1]));
  t.false(ShowService.checkShowHasDrm(shows[2]));
  t.false(ShowService.checkShowHasDrm(shows[3]));
});

test("checkShowHasDrm checks that the drm value is set to true, not just a truthy value", t => {
  const shows: IShow[] = [
    {
      ...getDefaultShow(),
      // @ts-ignore
      drm: 11
    },
    {
      ...getDefaultShow(),
      // @ts-ignore
      drm: "testing-123"
    },
    {
      ...getDefaultShow(),
      // @ts-ignore
      drm: {}
    },
    {
      ...getDefaultShow(),
      // @ts-ignore
      drm: []
    },
    {
      ...getDefaultShow(),
      drm: true
    }
  ];
  t.false(ShowService.checkShowHasDrm(shows[0]));
  t.false(ShowService.checkShowHasDrm(shows[1]));
  t.false(ShowService.checkShowHasDrm(shows[2]));
  t.false(ShowService.checkShowHasDrm(shows[3]));
  t.true(ShowService.checkShowHasDrm(shows[4]));
});

test("checkShowHasEpisodes returns shows correctly", t => {
  const shows: IShow[] = [
    {
      ...getDefaultShow(),
      episodeCount: null
    },
    {
      ...getDefaultShow(),
      episodeCount: undefined
    },
    {
      ...getDefaultShow()
    },
    {
      ...getDefaultShow(),
      // @ts-ignore
      episodeCount: "10 billion"
    },
    {
      ...getDefaultShow(),
      episodeCount: 10
    },
    {
      ...getDefaultShow(),
      episodeCount: 0
    }
  ];

  t.false(ShowService.checkShowHasEpisodes(shows[0], 1));
  t.false(ShowService.checkShowHasEpisodes(shows[1], 1));
  t.false(ShowService.checkShowHasEpisodes(shows[2], 1));
  t.false(ShowService.checkShowHasEpisodes(shows[3], 1));
  t.true(ShowService.checkShowHasEpisodes(shows[4], 1));
  t.false(ShowService.checkShowHasEpisodes(shows[5], 1));
});

test("checkShowHasEpisodes works with different minimumRequirement variable values", t => {
  const show: IShow = {
    ...getDefaultShow(),
    episodeCount: 5
  };

  t.true(ShowService.checkShowHasEpisodes(show, 2));
  t.true(ShowService.checkShowHasEpisodes(show, 4));
  t.true(ShowService.checkShowHasEpisodes(show, 5));
  t.false(ShowService.checkShowHasEpisodes(show, 6));
  t.false(ShowService.checkShowHasEpisodes(show, 111));
});

test("checkShowHasEpisodes works with scientific numbers", t => {
  const show: IShow = {
    ...getDefaultShow(),
    episodeCount: 1e2
  };

  t.true(ShowService.checkShowHasEpisodes(show, 90));
  t.false(ShowService.checkShowHasEpisodes(show, 110));
});

test("checkShowHasEpisodes does not have an off by one error", t => {
  const show: IShow = {
    ...getDefaultShow(),
    episodeCount: 5
  };

  t.true(ShowService.checkShowHasEpisodes(show, 4));
  t.true(ShowService.checkShowHasEpisodes(show, 5));
  t.false(ShowService.checkShowHasEpisodes(show, 6));
});

test("formatShow helpers formats down to the accepted format", t => {
  const show: IShow = {
    ...getDefaultShow(),
    image: {
      showImage: "https://testingimage.com"
    },
    episodeCount: 10,
    drm: true,
    nextEpisode: null
  };

  t.deepEqual(ShowService.formatShow(show), {
    title: getDefaultShow().title,
    slug: getDefaultShow().slug,
    image: "https://testingimage.com"
  });
});

test("filterShows filters and maps correclty", t => {
  const s = new ShowService();

  const shows: IShow[] = [
    {
      ...getDefaultShow(),
      episodeCount: 0,
      drm: true,
      image: {
        showImage: "https://cats.com/1/1"
      },
      language: "english",
      primaryColour: "red"
    },
    {
      ...getDefaultShow(),
      episodeCount: 10,
      drm: false,
      image: {
        showImage: "https://cats.com/2/2"
      },
      language: "english",
      primaryColour: "red"
    },
    {
      ...getDefaultShow(),
      episodeCount: 15,
      drm: true,
      image: {
        showImage: "https://cats.com/3/3"
      },
      language: "english",
      primaryColour: "red"
    }
  ];

  t.deepEqual(s.filterShows(shows), [
    {
      title: getDefaultShow().title,
      slug: getDefaultShow().slug,
      image: "https://cats.com/3/3"
    }
  ]);
});

test("filterShows returns null if a bad value is passed in", t => {
  const s = new ShowService();
  // @ts-ignore
  t.deepEqual(s.filterShows({}), null);
});
