import {
  controller,
  httpPost,
  // requestBody,
  BaseHttpController,
  // request,
  requestBody
} from "inversify-express-utils";
// import * as express from "express";
import { inject } from "inversify";
import { IJSONService } from "../services/JSONService";
import { IShowService } from "../services/ShowService";

/**
 * This is our root endpoint controller for the app "/"
 * We extend the BaseHttpController because it provides us
 * with a range of methods that set status codes for us.
 */
@controller("/")
export class RootController extends BaseHttpController {
  /**
   * Set up our dependencies for this controller.
   *
   * The idea being this is that in an enterprise application where you have many endpoints
   * and endpoint controllers, you might want to mock the service being passed in to
   * do something different in your integration tests. 😃
   */
  constructor(
    @inject("JSONService") private _JSONService: IJSONService,
    @inject("ShowService") private _ShowService: IShowService
  ) {
    super();
  }

  @httpPost("/")
  public async show(
    /** this decorator just gives us the value of the express `request.body` object */
    @requestBody() body: any
  ) {
    try {
      const validatedJSON = this._JSONService.validateJSONString(body);

      // if the payload doesn't exist we should exit early
      if (!validatedJSON.payload) {
        return this.json(
          {
            error: "Could not decode request: No payload was attached"
          },
          400
        );
      }

      const filteredShows = this._ShowService.filterShows(
        validatedJSON.payload
      );

      // filtered shows will only be null if no shows are provided
      if (filteredShows === null) {
        return this.json(
          {
            error:
              "Could not decode request: No shows were included in the payload"
          },
          400
        );
      }

      return this.ok({
        response: filteredShows
      });
    } catch (e) {
      if (e instanceof SyntaxError) {
        console.error(e);
        return this.json(
          { error: "Could not decode request: JSON parsing failed" },
          400
        );
      }
      console.error("ALERT: An unknown error has occured", e);
      return this.json(
        { error: "Could not decode request: An unknown error occured" },
        400
      );
    }
  }
}
