/**
 * A filtered and mapped down version of `IShow`
 */
export interface IFilteredShow {
  image: string;
  slug: string;
  title: string;
}
