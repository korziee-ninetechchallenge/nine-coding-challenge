interface Image {
  showImage: string;
}

interface NextEpisode {
  channel?: any;
  channelLogo: string;
  date?: any;
  html: string;
  url: string;
}

interface Season {
  slug: string;
}

/**
 * In the context of the 9Digital challenge, this represents the "incoming", or expectant data
 *
 * This interface was defined based on the `sample_request.json` file. Hence the reason the only
 * required properties are title, slug, and tvChannel.
 */
export interface IShow {
  country?: string;
  description?: string;
  drm?: boolean;
  episodeCount?: number;
  genre?: string;
  image?: Image;
  language?: string;
  nextEpisode?: NextEpisode;
  primaryColour?: string;
  seasons?: Season[];
  slug: string;
  title: string;
  tvChannel: string;
}
