// import reflect metadata polyfill early and in the root.
import "reflect-metadata";
import * as bodyParser from "body-parser";

import { InversifyExpressServer } from "inversify-express-utils";
import { challengeContainer } from "./config/inversify";

// instantiate the express server with our composition root container
const server = new InversifyExpressServer(challengeContainer);

// start the server 🎉 🎊
server
  .setConfig(app => {
    /**
     * The requirments of this tech challenge seem to want to see more control over the json validation,
     * so we are only using the body parser here to manipulate streams and once the request is completely received,
     * add it onto the `req.body` value for the request.
     *
     * Otherwise, I would simply let bodyParser handle validation and simply set the `req.body` to be the parsed json value.
     */
    app.use(bodyParser.text({ type: "*/*" }));
  })
  .build()
  .listen(process.env.PORT || 3000, () => {
    console.log(`Server running on port ${process.env.PORT || 3000}`);
  });
