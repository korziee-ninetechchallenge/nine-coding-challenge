import { Container } from "inversify";
import { IJSONService, JSONService } from "../services/JSONService";
import { IShowService, ShowService } from "../services/ShowService";

/**
 * Endpoint bindings, inversify looks at the metadata exported based on the
 * `controller` decoration we applied 😄
 */
import "../endpoints/index";

/**
 * Set up the Inversify dependency container and all of the bindings
 *
 * In an enterprise application, we might have hundreds of services/bindings,
 * and this file is usually the ingress point for all of these
 */
const challengeContainer = new Container();

// service binding
challengeContainer.bind<IJSONService>("JSONService").to(JSONService);
challengeContainer.bind<IShowService>("ShowService").to(ShowService);

export { challengeContainer };
